package com.example.harrypotterapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Spell implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("spell")
    @Expose
    private String spell;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("effect")
    @Expose
    private String effect;
    @SerializedName("__v")
    @Expose
    private Integer v;
    private final static long serialVersionUID = 3220293168714683143L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSpell() {
        return spell;
    }

    public void setSpell(String spell) {
        this.spell = spell;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

}
