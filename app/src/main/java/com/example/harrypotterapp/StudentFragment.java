package com.example.harrypotterapp;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.harrypotterapp.Model.Spell;
import com.example.harrypotterapp.Model.Student;
import com.example.harrypotterapp.ViewModels.SpellViewModel;
import com.example.harrypotterapp.ViewModels.StudentViewModel;
import com.example.harrypotterapp.adapters.SpellAdapter;
import com.example.harrypotterapp.adapters.StudentAdapter;

import java.util.List;
import java.util.Objects;

public class StudentFragment extends Fragment {
    StudentAdapter studentAdapter;
    RecyclerView studentsRecycler;
    StudentViewModel studentViewModel;

    public StudentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_student, container, false);
        studentsRecycler = view.findViewById(R.id.student_recycler_view);
        studentsRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        studentsRecycler.setHasFixedSize(true);

        studentViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(StudentViewModel.class);
        studentViewModel.init();
        studentViewModel.getStudentRepository().observe(this, new Observer<List<Student>>() {
            @Override
            public void onChanged(@Nullable List<Student> studentList) {
                studentAdapter = new StudentAdapter(getContext(), studentList);
                studentsRecycler.setAdapter(studentAdapter);
                studentAdapter.setStudentSelectedListener(StudentFragment.this::showStudentDetails);
            }
        });


        return view;
    }

    public void showStudentDetails(String studentId) {
        Intent intent = new Intent(getActivity(), StudentDetailActivity.class);
        intent.putExtra("studentId", studentId);
    }

}