package com.example.harrypotterapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.harrypotterapp.Model.House;
import com.example.harrypotterapp.R;

import java.util.List;

public class HouseAdapter extends RecyclerView.Adapter<HouseAdapter.HouseViewHolder> {
    Context context;
    List<House> houseList;
    private HouseSelectedListener houseSelectedListener;

    public HouseAdapter(Context context,List<House> houseList){
        this.context = context;
        this.houseList = houseList;
    }

    @NonNull
    @Override
    public HouseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_house,parent,false);
        return new HouseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HouseViewHolder holder, int position) {
        House house = houseList.get(position);

        holder.houseName.setText(house.getName());
        holder.houseHead.setText("Head of house: " + house.getHeadOfHouse());
        holder.houseGhost.setText("House Ghost:" + house.getHouseGhost());
        holder.houseFounder.setText("House founder: "+ house.getFounder());

        holder.itemView.setOnClickListener(view -> {
            if(houseSelectedListener != null){
                houseSelectedListener.onDisplayHouseDetails(house.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return houseList.size();
    }

    class HouseViewHolder extends RecyclerView.ViewHolder{
        TextView houseName, houseHead, houseGhost, houseFounder;

        public HouseViewHolder(@NonNull View itemView) {
            super(itemView);

            houseName = itemView.findViewById(R.id.house_name);
            houseHead = itemView.findViewById(R.id.house_head);
            houseGhost = itemView.findViewById(R.id.house_ghost);
            houseFounder = itemView.findViewById(R.id.house_founder);
        }
    }

    public interface HouseSelectedListener{
        void onDisplayHouseDetails(String houseId);
    }

    public void setHouseSelectedListener(HouseSelectedListener listener){
        houseSelectedListener = listener;
    }

}
