package com.example.harrypotterapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.harrypotterapp.Model.Student;
import com.example.harrypotterapp.R;

import java.util.List;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.StudentViewHolder> {
    Context context;
    List<Student> studentList;
    private StudentSelectedListener studentSelectedListener;

    public StudentAdapter(Context context, List<Student> studentList){
        this.context = context;
        this.studentList = studentList;
    }

    @NonNull
    @Override
    public StudentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_student,parent,false);
        return new StudentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentViewHolder holder, int position) {
        Student student = studentList.get(position);
        holder.studentName.setText(student.getName());
        holder.studentCardView.setOnClickListener(view -> {
            if(studentSelectedListener != null)
            {
                studentSelectedListener.onDisplayStudentDetails(student.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }

    class StudentViewHolder extends RecyclerView.ViewHolder{
        TextView studentName;
        CardView studentCardView;

        public StudentViewHolder(@NonNull View itemView) {
            super(itemView);
            studentName = itemView.findViewById(R.id.student_name);
            studentCardView  = itemView.findViewById(R.id.student_card_view);
        }
    }

    public interface StudentSelectedListener{
        void onDisplayStudentDetails(String studentId);
    }

    public void setStudentSelectedListener(StudentSelectedListener listener){
        studentSelectedListener = listener;
    }
}
