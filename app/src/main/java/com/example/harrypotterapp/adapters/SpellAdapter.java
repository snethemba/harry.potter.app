package com.example.harrypotterapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.harrypotterapp.Model.Spell;
import com.example.harrypotterapp.R;

import java.util.List;

public class SpellAdapter extends RecyclerView.Adapter<SpellAdapter.SpellViewHolder> {
    Context context;
    List<Spell> spellList;

    public SpellAdapter(Context context, List<Spell> spellList){
        this.context = context;
        this.spellList = spellList;
    }
    @NonNull
    @Override
    public SpellViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_spell,parent,false);
        return new SpellViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SpellViewHolder holder, int position) {
        Spell spell = spellList.get(position);

        holder.spellName.setText(spell.getSpell());
        holder.spellType.setText("Spell type: " + spell.getType());
        holder.spellEffect.setText("Spell effect" + spell.getEffect());
    }

    @Override
    public int getItemCount() {
        return spellList.size();
    }

    class SpellViewHolder extends RecyclerView.ViewHolder{

        TextView spellName, spellType, spellEffect;

        public SpellViewHolder(@NonNull View itemView) {
            super(itemView);

            spellName = itemView.findViewById(R.id.spell_name);
            spellType = itemView.findViewById(R.id.spell_type);
            spellEffect = itemView.findViewById(R.id.spell_effect);
        }
    }
}
