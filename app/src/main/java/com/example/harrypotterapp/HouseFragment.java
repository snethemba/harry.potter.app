package com.example.harrypotterapp;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.harrypotterapp.Model.House;
import com.example.harrypotterapp.ViewModels.HouseViewModel;
import com.example.harrypotterapp.adapters.HouseAdapter;

import java.util.List;
import java.util.Objects;

public class HouseFragment extends Fragment {

    HouseAdapter houseAdapter;
    RecyclerView housesRecycler;
    HouseViewModel houseViewModel;

    public HouseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_house, container, false);
        housesRecycler = view.findViewById(R.id.house_recycler_view);
        housesRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        housesRecycler.setHasFixedSize(true);

        houseViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(HouseViewModel.class);
        houseViewModel.init();
        houseViewModel.getHouseRepository().observe(this, new Observer<List<House>>() {
            @Override
            public void onChanged(@Nullable List<House> houseList) {
                houseAdapter = new HouseAdapter(getContext(), houseList);
                housesRecycler.setAdapter(houseAdapter);
                houseAdapter.setHouseSelectedListener(HouseFragment.this::showHouseMembers);
            }
        });

        return view;
    }

    public void showHouseMembers(String houseId) {
        Intent intent = new Intent(getActivity(), HouseMemberActivity.class);
        intent.putExtra("houseId", houseId);

    }
}