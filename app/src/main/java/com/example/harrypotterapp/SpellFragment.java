package com.example.harrypotterapp;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.harrypotterapp.Model.House;
import com.example.harrypotterapp.Model.Spell;
import com.example.harrypotterapp.ViewModels.HouseViewModel;
import com.example.harrypotterapp.ViewModels.SpellViewModel;
import com.example.harrypotterapp.adapters.HouseAdapter;
import com.example.harrypotterapp.adapters.SpellAdapter;

import java.util.List;
import java.util.Objects;

public class SpellFragment extends Fragment {

    RecyclerView spellsRecycler;
    SpellAdapter spellAdapter;
    SpellViewModel spellViewModel;

    public SpellFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_spell, container, false);
        spellsRecycler = view.findViewById(R.id.spell_recycler_view);
        spellsRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        spellsRecycler.setHasFixedSize(true);

        spellViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(SpellViewModel.class);
        spellViewModel.init();
        spellViewModel.getHouseRepository().observe(this, new Observer<List<Spell>>() {
            @Override
            public void onChanged(@Nullable List<Spell> spellList) {
                spellAdapter = new SpellAdapter(getContext(), spellList);
                spellsRecycler.setAdapter(spellAdapter);
            }
        });

        return view;
    }
}