package com.example.harrypotterapp.Network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PotterService {
    public static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://www.potterapi.com/v1/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public static <S> S createService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }
}
