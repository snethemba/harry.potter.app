package com.example.harrypotterapp.Network;

import androidx.lifecycle.MutableLiveData;

import com.example.harrypotterapp.Model.House;
import com.example.harrypotterapp.Model.Spell;
import com.example.harrypotterapp.Model.Student;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PotterApiRepository {

    public static PotterApiRepository potterApiRepository;

    public static PotterApiRepository getInstance() {
        if (potterApiRepository == null) {
            potterApiRepository = new PotterApiRepository();
        }
        return potterApiRepository;
    }

    private PotterApi potterApi;

    public PotterApiRepository(){
        potterApi = PotterService.createService(PotterApi.class);
    }

    public MutableLiveData<List<House>>getHouses (String key){
        MutableLiveData<List<House>> houses = new MutableLiveData<>();
        potterApi.getHouseList(key).enqueue(new Callback<List<House>>() {
            @Override
            public void onResponse(Call<List<House>> call, Response<List<House>> response) {
                if (response.isSuccessful()){
                    houses.setValue(response.body());
                }
            }
            @Override
            public void onFailure(Call<List<House>> call, Throwable t) {
                    houses.setValue(null);
            }
        });
        return houses;
    }

    public MutableLiveData<List<Student>> getStudents (String key){
        MutableLiveData<List<Student>> students = new MutableLiveData<>();
        potterApi.getStudentList(key).enqueue(new Callback<List<Student>>() {
            @Override
            public void onResponse(Call<List<Student>> call, Response<List<Student>> response) {
                if (response.isSuccessful()){
                    students.setValue(response.body());
                }
            }
            @Override
            public void onFailure(Call<List<Student>> call, Throwable t) {
                students.setValue(null);
            }
        });
        return students;
    }

    public MutableLiveData<List<Spell>> getSpells (String key){
        MutableLiveData<List<Spell>> spells = new MutableLiveData<>();
        potterApi.getSpellList(key).enqueue(new Callback<List<Spell>>() {
            @Override
            public void onResponse(Call<List<Spell>> call, Response<List<Spell>> response) {
                if (response.isSuccessful()){
                    spells.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Spell>> call, Throwable t) {
                spells.setValue(null);
            }
        });
        return spells;
    }

    public MutableLiveData<Student> getStudent(String key, String studentId){
        MutableLiveData<Student> student = new MutableLiveData<>();
        potterApi.getStudentDetails(key, studentId).enqueue(new Callback<Student>() {
            @Override
            public void onResponse(Call<Student> call, Response<Student> response) {
                student.setValue(response.body());
            }

            @Override
            public void onFailure(Call<Student> call, Throwable t) {
                student.setValue(null);
            }
        });
        return student;
    }

    public MutableLiveData<House> getHouse(String key, String houseId){
        MutableLiveData<House> house = new MutableLiveData<>();
        potterApi.getHouseDetails(key, houseId).enqueue(new Callback<House>() {
            @Override
            public void onResponse(Call<House> call, Response<House> response) {
                house.setValue(response.body());
            }

            @Override
            public void onFailure(Call<House> call, Throwable t) {
                house.setValue(null);
            }
        });
        return house;
    }

}
