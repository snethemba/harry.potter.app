package com.example.harrypotterapp.Network;

import com.example.harrypotterapp.Model.House;
import com.example.harrypotterapp.Model.Spell;
import com.example.harrypotterapp.Model.Student;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PotterApi {
    @GET("houses")
    Call<List<House>>getHouseList(@Query("key") String key);

    @GET("characters")
    Call<List<Student>> getStudentList(@Query("key") String key);

    @GET("spells")
    Call<List<Spell>> getSpellList(@Query("key") String key);

    @GET("characters/{characterId}")
    Call <Student> getStudentDetails(@Query("key") String key, @Query("characterId") String characterId);

    @GET("houses/{houseId}")
    Call<House>getHouseDetails(@Query("key") String key, @Query("houseId") String houseId);
}
