package com.example.harrypotterapp.ViewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.example.harrypotterapp.Model.Spell;
import com.example.harrypotterapp.Network.PotterApiRepository;

import java.util.List;

public class SpellViewModel extends ViewModel {

    private MutableLiveData<List<Spell>> spellMutableLiveData;

    public void init(){
        if(spellMutableLiveData!=null){
            return;
        }
        PotterApiRepository potterApiRepository = PotterApiRepository.getInstance();
        String apiKey = "$2a$10$1JEnmtEF417yBaFZcr51qukRjaKv8d5toEG5DKP/IUZWIVwfsaF7y";
        spellMutableLiveData = potterApiRepository.getSpells(apiKey);
    }

    public LiveData<List<Spell>> getHouseRepository(){
        return spellMutableLiveData;
    }
}
