package com.example.harrypotterapp.ViewModels;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.harrypotterapp.Model.Student;
import com.example.harrypotterapp.Network.PotterApiRepository;

import java.util.List;

public class StudentViewModel extends ViewModel {

    private MutableLiveData<List<Student>> studentMutableLiveData;

    public void init(){
        if(studentMutableLiveData!=null){
            return;
        }
        PotterApiRepository potterApiRepository = PotterApiRepository.getInstance();
        String apiKey = "$2a$10$1JEnmtEF417yBaFZcr51qukRjaKv8d5toEG5DKP/IUZWIVwfsaF7y";
        studentMutableLiveData = potterApiRepository.getStudents(apiKey);
    }

    public LiveData<List<Student>> getStudentRepository(){
        return studentMutableLiveData;
    }
}
