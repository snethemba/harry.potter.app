package com.example.harrypotterapp.ViewModels;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.example.harrypotterapp.Model.House;
import com.example.harrypotterapp.Network.PotterApiRepository;

import java.util.List;

public class HouseViewModel extends ViewModel {

    private MutableLiveData<List<House>> houseMutableLiveData;

    public void init(){
        if(houseMutableLiveData!=null){
            return;
        }
        PotterApiRepository potterApiRepository = PotterApiRepository.getInstance();
        String apiKey = "$2a$10$1JEnmtEF417yBaFZcr51qukRjaKv8d5toEG5DKP/IUZWIVwfsaF7y";
        houseMutableLiveData = potterApiRepository.getHouses(apiKey);
    }

    public LiveData<List<House>> getHouseRepository(){
        return houseMutableLiveData;
    }
}
